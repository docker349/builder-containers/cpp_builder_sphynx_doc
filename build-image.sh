#!/bin/bash

THIS_PATH=$(dirname $(realpath $0))

ENV_F=$THIS_PATH/env
if [[ -f $ENV_F ]]; then
    source $ENV_F
else
    echo "Error! env file not exist"
    exit 1
fi

docker build -t $CONT_NAME .
