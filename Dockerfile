FROM r6l025/cicppbuilder

MAINTAINER Bolshevik <bolshevikov.igor@gmail.com>

USER root

RUN apt-get update &&  DEBIAN_FRONTEND=noninteractive apt-get install -y -q \
    pip                             \
    python3.10-venv                 \
    doxygen                         \
    iputils-ping                    \
 && apt-get clean                   \
 && rm -rf /var/lib/apt/lists/*     \
 && ln -s /usr/bin/python3 /usr/bin/python

USER user
RUN python3 -m pip install -U sphinx &&         \
    python3 -m pip install sphinx-rtd-theme &&  \
    python3 -m pip install --user pipx &&       \
    python3 -m pipx ensurepath

WORKDIR ${HOME}
# install sphynx_rtd_dark_mode
COPY ./soft ./soft
USER root
RUN chown -R user:user ./soft
USER user
RUN    pushd ./soft/sphinx_rtd_dark_mode    \
    && ./rebuild.sh

RUN    pushd soft/renku-sphinx-theme        \
    && pip install -r docs/requirements.txt \
    && pip install --editable .

ENTRYPOINT ["/bin/bash", "-l", "-c"]
