FROM ubuntu

MAINTAINER Bolshevik <bolshevikov.igor@gmail.com>


RUN apt-get update &&  DEBIAN_FRONTEND=noninteractive apt-get install -y -q \
    apt-utils           \
    default-jre         \
    git                 \
    gnupg               \
    gzip                \
    iproute2            \
    libgtk2.0-0         \
    libncurses5-dev     \
    libselinux1         \
    libssl-dev          \
    libtool             \
    locales             \
    mc                  \
    net-tools           \
    pax                 \
    python3             \
    socat               \
    sudo                \
    tar                 \
    unzip               \
    vim                 \
    wget                \
    x11-utils           \
    xterm               \
    xvfb                \
    zlib1g-dev          \
    meson               \
    cmake               \
    libglib2.0-0        \
    libyaml-dev         \
    libsystemd-dev      \
    bash-completion     \
    pyflakes3           \
    pycodestyle         \
    python3-nose        \
    pandoc              \
 && apt-get clean           \
 && rm -rf /var/lib/apt/lists/*

RUN locale-gen en_US.UTF-8 && update-locale

#make a user
RUN adduser --disabled-password --gecos '' user && \
    usermod -aG sudo user && \
    echo "user ALL=(ALL) NOPASSWD: ALL" >> /etc/sudoers

# Пароль пользователя
RUN sudo usermod -p $(openssl passwd cepstrum) user

# make /bin/sh symlink to bash instead of dash:
RUN echo "dash dash/sh boolean false" | debconf-set-selections
RUN DEBIAN_FRONTEND=noninteractive dpkg-reconfigure dash
RUN sudo ln -fs /bin/bash /bin/sh

USER user
ENV HOME /home/user
ENV LANG en_US.UTF-8

#
# Установка дополнительного софта и конфигурационных файлов
#
WORKDIR ${HOME}
COPY ./soft ./soft
# конфигурация vim
RUN  ./soft/vim/install.sh
RUN  cat ./soft/bash/bashrc_extend >> ~/.bashrc
# конфигурация mc
RUN  mkdir -p ~/.config/; cp -r ./soft/mc ~/.config/
RUN  sudo mkdir -p /root/.config/; sudo cp -r ./soft/mc /root/.config/
# почистим за собой
RUN  sudo rm -rf ./soft

# Setup python3 as default python
RUN echo 'alias python=python3' >> ~/.bashrc
# Set locale conf
RUN echo 'export LANG=en_US.UTF-8' >> ~/.bashrc
RUN sudo cp ~/.bashrc /root/


ENTRYPOINT ["/bin/bash"]
