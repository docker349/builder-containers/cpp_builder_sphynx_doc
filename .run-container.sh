#!/bin/bash

if [ ! -d $1 ]; then
    echo "Error! Building dir not exist"
    exit 1
fi
BUILDDIR=$1
shift

THIS_PATH=$(dirname $(realpath $0))

ENV_F=$THIS_PATH/env
if [[ -f $ENV_F ]]; then
    source $ENV_F
else
    echo "Error! env file not exist"
    exit 1
fi

docker run                              \
    -ti                                 \
    --rm                                \
    -v $BUILDDIR:/home/user/builddir/   \
    $CONT_NAME                          \
    $@
exit $?
